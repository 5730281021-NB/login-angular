var express = require('express');
var bodyParser = require('body-parser')
var router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';

const dbName = 'usercenter';


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/register', (req, res, next) => {
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var email = req.body.email;
  var userid = req.body.userid;
  var username = req.body.username;
  var password = req.body.password;
  var department = req.body.department;
  var position = req.body.position;
  var sex = req.body.sex;

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
   
    const db = client.db(dbName);
   
    insertDocuments(firstname,lastname,email,userid,username,password,department,position,sex,db, function() {
      client.close();
    });
  });
  res.send('finish register');
});

router.get('/login', (req, res, next) => {
  var userid = req.body.userid;
  var username = req.body.username;
  var password = req.body.password;

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
   
    const db = client.db(dbName);
   
    findDocuments(userid,username,password,db,res, function() {
      client.close();
    });
  });
});

router.get('/getuser', (req, res, next) => {
  var userid = req.body.userid;

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
   
    const db = client.db(dbName);
   
    findDocumentsbyid(userid,db,res, function() {
      client.close();
    });
  });
});

router.get('/alluser', (req, res, next) => {

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
   
    const db = client.db(dbName);
   
    findAllDocuments(db,res, function() {
      client.close();
    });
  });
});


const findDocuments = function(userid,username,password,db,res, callback) {
  // Get the documents collection
  const collection = db.collection('users');
  // Find some documents
  collection.find({"userid": userid,"username": username,"password":password}).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs);
    if(docs.length < 1) res.send('Not Found User in DB');
    else {res.send('LOGIN SUCCESSFUL');
    callback(docs);
    }
  });
}
const findDocumentsbyid = function(userid,db,res, callback) {
  // Get the documents collection
  const collection = db.collection('users');
  // Find some documents
  collection.find({"userid": userid}).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs);
    if(docs.length < 1) res.send('Not Found User in DB');
    else {res.json(docs[0]);
    callback(docs);
    }
  });
}

const findAllDocuments = function(db,res, callback) {
  // Get the documents collection
  const collection = db.collection('users');
  // Find some documents
  collection.find().toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs);
    if(docs.length < 1) res.send('Not Found User in DB');
    else {res.json(docs);
    callback(docs);
    }
  });
}


const insertDocuments = function(firstname,lastname,email,userid,username,password,department,position,sex,db, callback) {
  // Get the documents collection
  const collection = db.collection('users');
  // Insert some documents
  collection.insertMany([
    {firstname : firstname,lastname:lastname,email:email,userid : userid,username : username,password : password,department : department,position :position, sex :sex},
    {firstname : 'benjarat',lastname:'chongpao',email:'benjarat@booking.com',userid : 'benben',username : 'benjarat',password : 'bookbook',department : 'MSC/DTS',position :'DEV', sex :'female'},
    {firstname : 'naphol',lastname:'aya',email:'naphol@hotmail.com',userid : 'napholaya',username : 'naphol',password : 'imfine',department : 'MSC/DTS',position :'DEV', sex :'male'},
    {firstname : 'nontouch',lastname:'boonyamanond',email:'jeff@hotmail.com',userid : 'jeff123',username : 'nontouch',password : 'xd12345',department : 'MSC/DTS',position :'DEV', sex :'male'},
    {firstname : 'jettapat',lastname:'thitaram',email:'nopnopnop@metrosystems.co.th',userid : 'nopu',username : 'jettapat',password : '123789456',department : 'MSC/DTS',position :'DEV', sex :'male'}
  ]
  , function(err, result) {
    assert.equal(err, null);
    assert.equal(5, result.result.n);
    assert.equal(5, result.ops.length);
    console.log("Inserted 5 documents into the collection");
    callback(result);
  });
}
module.exports = router;
